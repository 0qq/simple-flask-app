# Dockerfile

FROM python:3.8-slim

WORKDIR /service
EXPOSE 9000

COPY src/requirements.txt ./
RUN pip install -r requirements.txt

ENV FLASK_APP=app/main.py
COPY src/ ./

RUN python -m pytest

ENTRYPOINT ["python", "-m"]
CMD ["app.main"]
